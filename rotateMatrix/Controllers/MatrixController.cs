﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using rotateMatrix.Models;
using System.IO;
using System.Text;
using System.Diagnostics;


namespace rotateMatrix.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class MatrixController : Controller
    {
        [Route("api/rotate")]
        [HttpPost]
        public IActionResult Rotate(Matrix matrix)
        {
            matrix.rotate90();
            return Json(matrix);
        }

        [Route("upload")]
        [HttpPost, DisableRequestSizeLimit]
        public IActionResult UploadFile()
        {
            var file = Request.Form.Files[0];
            var reader = new StreamReader(file.OpenReadStream()).ReadToEnd();
            var csv = ParsingCsv.ParsingString(reader);
            return Json(csv);
        }
        [Route("download")]
        [HttpPost]
        public FileResult DownloadFile(Matrix matrix)
        {
            var csvString = ParsingCsv.ParsingMatrixBody(matrix.body);
            byte[] csv = Encoding.ASCII.GetBytes(csvString);
            string file_type = "text/csv";
            string file_name = "matrix.csv";
            return File(csv, file_type, file_name);
        }

    }
}
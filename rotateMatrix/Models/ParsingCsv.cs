﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace rotateMatrix.Models
{
    public static class ParsingCsv
    {
        public static double[,] ParsingString(string baseX)
        {
            string[] lines = baseX.Split("\n");
            double[,] x = new double[lines.Length, Regex.Matches(lines[0], @"[0-9]+(\.[0-9]+)?").Count];
            for (int i = 0; i < lines.Length; i++)
            {
                var reg = Regex.Matches(lines[i], @"[0-9]+(\.[0-9]+)?");
                for (int j = 0; j < reg.Count; j++)
                {
                    x[i, j] = Double.Parse(reg[j].Value);
                }
            }
            return x;
        }

        public static string ParsingMatrixBody(double[,] body)
        {
            int Length = body.GetLength(0);
            string s = "";
            for (int i = 0; i < Length; i++)
            {
                for (int j = 0; j < Length - 1; j++)
                {
                    s += body[i, j].ToString() + ",";
                }
                s+= i != Length - 1 ?  body[i, Length - 1].ToString() + "\n" :  body[i, Length - 1].ToString();
            }
            return s;
        }
    }
}

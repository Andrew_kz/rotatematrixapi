﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rotateMatrix.Models
{
    public class Matrix
    {
        public double[,] body;

        public void rotate90()
        {
            double temp;
            int length = body.GetLength(0);
            for (int i = 0; i < length / 2; i++)
            {
                for (int j = i; j < length - 1 - i; j++)
                {
                    temp = body[i, j];
                    body[i, j] = body[length - j - 1, i];
                    body[length - j - 1, i] = body[length - i - 1, length - j - 1];
                    body[length - i - 1, length - j - 1] = body[j, length - i - 1];
                    body[j, length - i - 1] = temp;
                }
            }
        }
    }
}
